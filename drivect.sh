.gdrive about

# get source link
source=$1

# get wget's log
dllog=$(wget --content-disposition -nv "$source" 2>&1)

# show log
echo "$dllog"

# get downloaded file name
file=$(echo "$dllog" | grep -o -P '(?<=").*(?=")')

# empty line
echo

# upload to drive (home directory)
./gdrive upload "$file"

# remove file
rm "$file"
